#include "Task_Michal_Lewicki.h"
#include <string>
#include <cctype>

std::string repeatStringNTimes(std::string s, size_t n){
	if (n == 1 || s.empty()) {
		return s;
	}
	size_t stringLength = s.size();
	if (stringLength == 1) {
		s.append(n - 1, s.front());
		return s;
	}
	s.reserve(stringLength * n);
	size_t timesRepeated = 2;
	for (; timesRepeated < n; timesRepeated *= 2) s += s;
	s.append(s.c_str(), (n - (timesRepeated / 2)) * stringLength);
	return s;
}

size_t getMatchingRightBracketPosition(std::string s) {
	int numberOfLeftBrackets = 0, numberOfRightBrackets = 0, bracketPosition = 0;
	do {
		bracketPosition = s.find_first_of("[]", bracketPosition + 1);
		s[bracketPosition] == '[' ? numberOfLeftBrackets++ : numberOfRightBrackets++;
	} while (numberOfLeftBrackets != numberOfRightBrackets);
	return bracketPosition;
}

std::string Task_Michal_Lewicki::UnpackString(std::string s) {
	std::string unpackedString = "", stringToAdd = "";
	while (s.length()) {
		if (!std::isdigit(s[0])) {
			size_t digitPosition = s.find_first_of("1234567890");
			stringToAdd = s.substr(0, digitPosition);
			s.erase(0, digitPosition);
		}
		else {
			size_t leftBracketPosition = 0, rightBracketPosition = 0, numberOfRepeats;
			numberOfRepeats = std::stoi(s, &leftBracketPosition);
			rightBracketPosition = getMatchingRightBracketPosition(s);
			stringToAdd = this->UnpackString(s.substr(leftBracketPosition + 1, rightBracketPosition - leftBracketPosition - 1));
			stringToAdd = repeatStringNTimes(stringToAdd, numberOfRepeats);
			s.erase(0, rightBracketPosition + 1);
		}
		unpackedString += stringToAdd;
	}
	return unpackedString;
}